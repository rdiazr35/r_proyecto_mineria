library(RPostgreSQL)
library(DBI)
Sys.setlocale("LC_ALL", "en_US.UTF-8")
pg = dbDriver("PostgreSQL")

con = dbConnect(pg, user = 'postgres', password = '12345', host= "localhost", port= 5432, dbname= "dbMineria")
postgresqlpqExec(con, "SET client_encoding = 'windows-1252'")
dbListTables(con) #LISTADO DE TABLAS EXISTENTES EN LA BASE DE DATOS 

facturas <- dbGetQuery(con, 'SELECT * FROM factura utf8') #EJEMPLO DE CARGADO DE DATOS DE UNA TABLA
#View(facturas)
server <- function(input, output) {
    #GENERAL
    output$Clientes <- renderValueBox({
        clientes <- facturas %>%
            group_by(cod_cliente) %>%
            n_distinct()
        
        valueBox(
            paste0(clientes),
            "Cliente totales", 
            icon = icon("user-friends"),
            color = "blue"
        )
    })
    
    output$MontoVentasMayor <- renderValueBox({
        monto_ventas <- max(facturas$monto) %>% 
            to_currency(currency_symbol = "", symbol_first = TRUE,
                        group_size = 3, group_delim = " ", 
                        decimal_size = 2, decimal_delim = ".")
        
        valueBox(
            paste0(monto_ventas),
            "Venta más grande",
            icon = icon("credit-card"),
            color = "green"
        )
    })
    
    output$MontoVentasMenor <- renderValueBox({
        monto_ventas <- min(facturas$monto) %>% 
            to_currency(currency_symbol = "", symbol_first = TRUE,
                        group_size = 3, group_delim = " ", 
                        decimal_size = 2, decimal_delim = ".")
        
        valueBox(
            paste0(monto_ventas),
            "Venta más pequeña",
            icon = icon("credit-card"),
            color = "red"
        )
    })
    #GENERAL
    
    # PROMEDIO DE VENTAS POR TIPO DE CLIENTE
    output$promedio_ventas <-  renderPlot({
        promedios <- facturas %>%
            group_by(desc_tipo_cliente) %>%
            summarise(prom = mean(monto))
        
        ggplot(promedios, aes(x = factor(desc_tipo_cliente), y = prom,
                              fill = as.factor(desc_tipo_cliente))) +
            geom_bar(stat = "identity") +
            xlab("Tipo Cliente") + ylab("Promedio ventas") +
            theme_classic(base_size = 10) +
            theme(legend.position = "none",
                  axis.text.x = element_text(angle = 90)) +
            geom_text(aes(label=prom)) + coord_flip()
        
    })
    
    # TIPOS DE CLIENTES MEJOR VENDIDO
    output$tipo_cliente_mejor_vendido <- renderPlot({
        mejor_vendido <- facturas %>%
            group_by(desc_tipo_cliente) %>%
            summarise(
                promedio = mean(monto)
            ) %>%
            arrange(desc(promedio)) %>%
            head(6)
        
        
        ventas_mayores_completos <- semi_join(facturas, mejor_vendido,
                                                by = "desc_tipo_cliente")
        
        # "Gerentes y subgerentes generales, directores y subdirectores generales y coordinadores generales de instituciones públicas y de empresas privadas"
        # "Personal de nivel directivo de la administración pública"
        
        ventas_mayores_completos <- ventas_mayores_completos %>%
            mutate(desc_tipo_cliente = replace(desc_tipo_cliente,
                                              str_detect(desc_tipo_cliente, "Acueducto"),
                                              "Centro turístico")) %>% 
            mutate(desc_tipo_cliente = replace(desc_tipo_cliente,
                                              str_detect(desc_tipo_cliente, "Cooperativa"),
                                              "Ferreteria"))
        
        ggplot(ventas_mayores_completos, aes(x = desc_tipo_cliente, y = monto,
                                               color = desc_tipo_cliente)) +
            geom_boxplot() +
            geom_jitter(alpha = 0.2) +
            xlab("Tipo cliente") +
            theme_classic(base_size = 16) +
            theme(legend.position = "none",
                  axis.text.x = element_text(angle = 90))
        
    })
    
    # Cuadros de los tipos de cliente peor vendidos
    output$menor_vendido <- DT::renderDataTable({
        cuadro_menor_vendido <- DT::datatable(facturas %>%
                                                  group_by(desc_tipo_cliente) %>%
                                                  summarise(promedio = mean(monto)) %>%
                                                  arrange(promedio) %>%
                                                  head(20)
        )
        
        formatCurrency(cuadro_menor_vendido, columns = "promedio", currency = "₡", 
                       interval = 3, mark = " ", digits = 2)
    })
    
    # PROMEDIO DE VENTAS POR RECURSO
    output$promedio_ventas_recurso <-  renderPlot({
        promedios <- facturas %>%
            group_by(desc_recurso) %>%
            summarise(prom = mean(monto)) %>%
            head(6)
        
        
        ggplot(promedios, aes(x = factor(desc_recurso), y = prom,
                              fill = as.factor(desc_recurso))) +
            geom_bar(stat = "identity") +
            xlab("Recurso") + ylab("Promedio ventas") +
            theme_classic(base_size = 16) +
            theme(legend.position = "none",
                  axis.text.x = element_text(angle = 90))+
            geom_text(aes(label=prom))
        
    })
    
    # PROMEDIO DE VENTAS POR PROVINCIA
    output$promedio_ventas_provincia <-  renderPlot({
        promedios <- facturas %>%
            group_by(desc_provincia) %>%
            summarise(prom = mean(monto))
        
        
        ggplot(promedios, aes(x = factor(desc_provincia), y = prom,
                              fill = as.factor(desc_provincia))) +
            geom_bar(stat = "identity") +
            xlab("Provincia") + ylab("Promedio ventas por provincia") +
            theme_classic(base_size = 16) +
            theme(legend.position = "none",
                  axis.text.x = element_text(angle = 90)) +
            geom_text(aes(label=prom))
        
    })
}




