  library(shinydashboard)

skin <- Sys.getenv("DASHBOARD_SKIN")
skin <- tolower(skin)
if (skin == "")
  skin <- "blue"


sidebar <- dashboardSidebar(
  sidebarSearchForm(label = "Search...", "searchText", "searchButton"),
  sidebarMenu(
    menuItem("Dashboard", tabName = "dashboard", icon = icon("dashboard"))
    )
)

body <- dashboardBody(
  
  tabItems(
    tabItem("dashboard",
            # Boxes with solid headers
            fluidRow(
              box(
                title = "Primer KPI", width = 4, height = 300, solidHeader = TRUE, status = "primary"
              ),
              box(
                title = "Segundo KPI", width = 4, height = 300, solidHeader = TRUE, status = "danger"
              ),
              box(
                title = "Tercer KPI", width = 4, height = 300, solidHeader = TRUE, status = "success"
              )
            ),
            fluidRow(
              box(
                title = "Cuarto KPI", width = 4, height = 300, solidHeader = TRUE, status = "info"
              ),
              box(
                title = "Quinto KPI", width = 4, height = 300, solidHeader = TRUE, status = "warning"
              ),
              box(
                title = "Sexto KPI", width = 4, height = 300, solidHeader = TRUE, status = "primary"
              )
            )
          )
        )
      )
#DATOS DEL ENCABEZADO
header <- dashboardHeader(
  title = "Proyecto de Minería"
)
#CARGADO DE UI PARA EL DASHBOARD
ui <- dashboardPage(header, sidebar, body, skin = skin)

shinyApp(ui, server)